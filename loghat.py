#!/usr/bin/python3
from robobrowser import RoboBrowser as rb
import re
import sys
from tabulate import tabulate


bb = rb(parser='html.parser')
html = "https://www.loghatnameh.de/old-version/"
bb.open(html)
x = bb.get_form()
x.fields["word"].value=sys.argv[1]
bb.submit_form(x)
# TODO timeout
try:
    words = bb.find("tr").find_all("a")
except Exception:
    print("Wort ist ungültig")
    exit(1)
list_of_words=[]
for word in words:      
    if re.match(r'<a href="/old-version/toSearchWordLink.do*' , str(word)):
        list_of_words.append( word.get_text() )
table=[]
headers = ["Wort", "Bedeutung"]
for j in range(0 , len(list_of_words) ,2):
    table.append([list_of_words[j] , list_of_words[j+1]])
print(tabulate(table, headers, tablefmt="grid",stralign="center"))
